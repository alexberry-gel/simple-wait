#!/bin/bash

count=1
exitcode=1

while [ $exitcode -ne 0 ] && [ $count -le $maxcount ]
do
  echo "attempt $count to connect to $dnsname on port $port"
  nc -zv $dnsname $port -w $timeout
  exitcode=$?
  if [ $exitcode -eq 0 ]
  then
    exit 0
  fi
  count=$(($count+1))
done

echo "Loop failed at attempt $(($count-1))"
exit 1
