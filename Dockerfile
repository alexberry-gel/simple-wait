FROM ubuntu:18.04

ENV dnsname=google.co.uk
ENV port=80
ENV timeout=2
ENV maxcount=10

WORKDIR /opt/wait

RUN apt update && \
    apt install netcat -y && \
    apt clean all

COPY wait.sh .

CMD ./wait.sh
