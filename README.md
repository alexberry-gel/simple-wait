# Simple netcat wait loop

This is a simple script to wait for a port to be listening before continuing.

## Parameters

```
# DNS Name to connect to
ENV dnsname=google.co.uk
# Port to connect to
ENV port=80
# netcat timeout
ENV timeout=2
# max attempts
ENV maxcount=10
```

## Building

`docker build . -t wait`

## Running

`docker run -e dnsname=bio-cva-docker-01.gel.zone -e port=80 -e timeout=2 -e maxcount=5 wait`
